<?php

class View
{
	public static function make($filename, $data = [])
	{
		$path = get_path('app').'/views/'.$filename.'.php';

		ob_start();

		extract($data);
		unset($data);

		require $path;

		return ob_get_clean();
	}
}