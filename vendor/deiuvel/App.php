<?php

class App
{
	protected static $error;

	public static function run()
	{
		$callback = Route::getRoute(get_current_route());

		echo call_user_func($callback);
	}

	public static function error($output)
	{
		static::$error = $output;
	}

	public static function getError()
	{
		return static::$error;
	}
}